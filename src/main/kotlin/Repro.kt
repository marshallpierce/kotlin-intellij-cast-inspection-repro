import java.util.concurrent.CancellationException
import java.util.concurrent.CompletableFuture
import java.util.concurrent.CompletionException

sealed class Maybe<T>
class Existing<T>(val value: T) : Maybe<T>()
class Missing<T> : Maybe<T>()

fun main() {
    val future = CompletableFuture.completedFuture("string")
            // IntelliJ incorrectly flags this cast as unnecessary
            .thenApply { Existing(it) as Maybe<String> }
            .getNowSwallowException<Maybe<String>>(Existing("default"))
}

fun <T> CompletableFuture<T>.getNowSwallowException(defaultValue: T): T {
    return try {
        getNow(defaultValue)
    } catch (e: CompletionException) {
        defaultValue
    } catch (e: CancellationException) {
        defaultValue
    }
}
